from django.urls import path
from .views import show_all_receipts

urlpatterns = [
    path("", show_all_receipts, name="home")
]
