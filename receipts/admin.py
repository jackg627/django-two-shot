from django.contrib import admin
from .models import Receipt, ExpenseCategory, Account

admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
