from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account


@login_required
def show_all_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receiptlist": receipts}
    return render(request, "receipt_lists/receipt.html", context)
